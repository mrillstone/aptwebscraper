using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AptProcessor.Common.Models;
using static AptProcessor.Common.Constants;

namespace AptProcessor.Scraper
{
    public class ZillowHtmlScraper
    {
        readonly IBrowsingContext _browsingContext;

        public ZillowHtmlScraper(IBrowsingContext browsingContext)
        {
            this._browsingContext = browsingContext;
        }

        public async Task<ISet<ZillowDownloadDto>> Run()
        {
            var htmlDocs = await DownloadHtmlDocuments();
            var parsedDocs = ParseHtmlDocuments(htmlDocs);

            return parsedDocs;
        }

        Task<IDocument []> DownloadHtmlDocuments() =>
            Task.WhenAll(
                _browsingContext.OpenAsync(CURRENT_LISTS_URL)
            );

        ISet<ZillowDownloadDto> ParseHtmlDocuments(IDocument[] docs) =>
            docs.Aggregate(
                new HashSet<ZillowDownloadDto>(),
                (docList, doc) => new HashSet<ZillowDownloadDto>(docList.Concat(ParseHtmlDocument(doc)))
            );

        static ISet<ZillowDownloadDto> ParseAptsLiElements (IEnumerable<IElement> elems) =>
            elems.Aggregate(new HashSet<ZillowDownloadDto>(), (aptsList, li) =>
            {
                var href = li.QuerySelector("a").GetAttribute("href");

                var downloadDto = new ZillowDownloadDto(
                    href
                );

                aptsList.Add(downloadDto);

                return aptsList;
            });

        static ISet<ZillowDownloadDto> ParseHtmlDocument(IDocument doc) =>
            ParseAptsLiElements(SelectLiElements(doc));

        static IEnumerable<IElement> SelectLiElements(IDocument doc) =>
            doc.QuerySelectorAll("article.list-card");

       
        
    }
}