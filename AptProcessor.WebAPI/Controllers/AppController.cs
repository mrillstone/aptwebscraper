using System;
using System.Linq;
using System.Threading.Tasks;
using AptProcessor.Scraper;
using Microsoft.AspNetCore.Mvc;

namespace AptProcessor.WebAPI.Controllers
{
    [ApiController, Route("~/")]
    public class AppController : ControllerBase
    {
        public async Task<ActionResult> Index([FromServices] ZillowHtmlScraper scraper)
        {
            var startTime = DateTime.Now;
            //TODO: implement scraper and parser logic
            var apts = await scraper.Run();


            return Ok(
                $"All done! Job ran at: {startTime}" +
                Environment.NewLine +
                Environment.NewLine +
                string.Join(Environment.NewLine, apts.Select(x => x.Href))
                );
        }
    }
}