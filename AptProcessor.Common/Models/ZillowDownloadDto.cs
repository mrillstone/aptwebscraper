using System;
using System.Linq;

namespace AptProcessor.Common.Models
{
    public class ZillowDownloadDto
    {
        private readonly string _rootLocation;

        public ZillowDownloadDto(string href, string price, string rooms, string squareFeet, string location, string validFrom, string rootLocation)
        {
            this.Href = href;
            this.Price = price;
            this.Rooms = rooms;
            this.SquareFeet = squareFeet;
            this.Location = location;
            this.ValidFrom = DateTime.Parse(validFrom);
            this._rootLocation = rootLocation;
        }

        public ZillowDownloadDto(string href)
        {
            this.Href = href;
        }

        public string FileName =>
            ValidFrom.ToString("yy-MM-dd_") +
            (Href.Split('/').LastOrDefault() ?? Href.Replace("/", "_").Replace(":", "_")).TrimEnd();

        public string Href { get; set; }
        public string Price { get; internal set; }
        public string Rooms { get; internal set; }
        public string SquareFeet { get; internal set; }
        public string Location { get; internal set; }
        public DateTime ValidFrom { get; private set; }

    }
}