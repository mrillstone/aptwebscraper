namespace AptProcessor.Common
{
    public static class Constants
    {
        public const string CURRENT_LISTS_URL = "https://www.zillow.com/homes/for_rent/?searchQueryState={%22isMapVisible%22:true,%22mapBounds%22:{%22west%22:-105.36460246679684,%22east%22:-104.34562053320309,%22south%22:39.39192169499449,%22north%22:40.135077474359726},%22isListVisible%22:true,%22pagination%22:{},%22filterState%22:{%22price%22:{%22min%22:0,%22max%22:348276},%22monthlyPayment%22:{%22min%22:0,%22max%22:1300},%22onlyRentalSmallDogsAllowed%22:{%22value%22:true},%22isForSaleByAgent%22:{%22value%22:false},%22isForSaleByOwner%22:{%22value%22:false},%22isNewConstruction%22:{%22value%22:false},%22isForSaleForeclosure%22:{%22value%22:false},%22isComingSoon%22:{%22value%22:false},%22isAuction%22:{%22value%22:false},%22isPreMarketForeclosure%22:{%22value%22:false},%22isPreMarketPreForeclosure%22:{%22value%22:false},%22isForRent%22:{%22value%22:true}}}";
		public const string DOWNLOAD_DIR = "";
    }
}